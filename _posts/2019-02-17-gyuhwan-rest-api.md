---
layout: post
title:  "REST API"
date:   2019-02-13 00:00:00 0000
author : "Gyuhwan Kim"
categories: network
---

## What is Rest API?

REST는 Representational State Transfer의 약자로서 2000년도에 Roy Thomas Fielding이라는 사람이 낸 [논문](https://www.ics.uci.edu/~fielding/pubs/dissertation/fielding_dissertation.pdf)에서 최초로 소개된 용어입니다. 저자는 해당 논문에서 당시 아키텍쳐가 웹의 본래 설계 우수성을 활용하지 못하고 있다고 판단하여, 웹의 장점을 최대한 활용할 수 있는 아키텍처 REST에 대해서 설명하고 있습니다.

### REST Component

REST는 다음과 같이 3가지 요소로 구성된다.

1. 자원 (Resource) : URI (ex. /data/data_id)
   - 서버는 모든 자원에 고유한 ID를 부여해야 하며, 사용자가 URI를 통해 해당 자원에 접근이 가능해야 한다.
   - 예를 들면, /member/1은 one-based indexing이라고 할 때, 1번째 멤버를 의미한다.
2. 행위 (Verb) : HTTP Method
   - GET, POST, PUT, DELETE와 같은 메소드를 제공한다.
   - 위에서 설명한 자원 URI은 메소드에 따라 기능이 달라질 수 있다.  GET의 경우 데이터를 제공하는 메소드로 쓰고, POST는 데이터를 생성하는데 사용, PUT은 데이터를 수정, DELETE는 데이터를 제거하는 용도로 사용한다.
3. 표현 (Representation) 
   - 데이터를 주고받을 때의 데이터의 표현 방식을 의미하며, 일반적으로  JSON과 XML으로 주고 받는 것이 일반적이지만 경우에 따라 TEXT, RSS 등등의 여러 형태로 표현할 수도 있다.

> 즉, REST의 용어에 대해서 정의하자면 HTTP URI를 통해 사용자에게 자원을 명시하고 HTTP Method를 통해 자원에 대해서 CRUD Operation을 적용하여 사용하는 아키텍쳐라고 할 수 있다.

###  REST Properties

