---
layout: post
title:  "2019년 2월 8일"
date:   2019-02-08 00:00:00 0000
author : "Gyuhwan Kim"
categories: diary
mathjax : true
---

설 연휴 동안 너무 놀아서 공부를 못했다.. 내일부터 다시 제대로 포스팅을 시작할 것이고, 오늘은 안드로이드 공부만 할 예정이다.

## 안드로이드 공부 (코틀린)

오늘은 안드로이드 생존코딩 8 ~ 9장에 있는 실습 코드를 작성할 예정이다. 각각 수평 측정기, 전자액자에 대한 내용을 다루고 있다. 예전과 마찬가지로 작성한 코드는 Github에 public으로 업로드한다.

### 8장 수평 측정기

8장에서는 [**액티비티의 생명주기**](https://developer.android.com/guide/components/activities)에 대해서 다루고 있다. 액티비티의 생명주기는 각 상황(액티비티 시작, 종료, 재개)에 따라 호출되는 콜백 메서드에 대한 설명이며, 매우 중요한 개념이다. 이를 정확하게 알아야 각 상황에 맞는 최적의 설계가 가능하다. 예를 들면 프로그램이 종료될 때 메모리 관리 문제라던지, 재개되었을 때 리소스 관리 문제를 표현할 수 있다. 이를 제대로 다루지 못한 프로그램이라면 예상치 못한 버그를 만나게 될 것이다. 그리고 [**센서의 사용법**](https://developer.android.com/reference/android/hardware/Sensor)에 대해서도 설명하고 있다. 그리고 로그 기능에 대해서도 알려주고 있따. 로그는 디버깅, 에러, 경고, 정보성 로그, 모든 로그 등을 표시할 수 있다. 여기서는 디버깅용 로그를 사용하였다. 그리고 커스텀 뷰를 만드는 방법에 대해서 설명하고 있다. 프로젝트 코드는 [**여기**](https://github.com/kimkyuhwan/Android-Kotlin-Example/tree/master/TiltSensor)에서 확인 가능하며 실행화면은 다음과 같다.

![image-20190122105243001](/jekyll/assets/img/2019-02-06-gyuhwan-android-08-01.jpeg)

### 9장 전자액자

9장에서는 **[콘텐츠 프로바이더](https://developer.android.com/guide/topics/providers/content-providers)**의 사용법에 대해서 설명하고 있다. 콘텐츠 프로바이더는 앱의 데이터 접근을 다른 앱에 허용해주는 컴포넌트이다. 그리고 저장소에는 내부 저장소, 외부 저장소가 있으며, 내부 저장소는 OS가 설치된 영역으로 유저가 접근할 수 없는 시스템 영역을 말한다. 앱이 사용하는 정보와 데이터베이스가 여기에 저장된다. 외부 저장소는 유저가 사용하는 영역으로 사진과 동영상등은 모두 외부 저장소에 저장된다. 그리고 **[권한](https://developer.android.com/guide/topics/manifest/uses-permission-element)**을 매니페스트에 추가하는 방법에 대해서도 설명한다. 또한 **[프래그먼트와 생명주기](https://developer.android.com/guide/components/fragments)**에 대해서도 설명하고, [**ViewPager**](https://developer.android.com/reference/android/support/v4/view/ViewPager)에 대해서도 설명한다. 프로젝트 코드는 [**여기**](https://github.com/kimkyuhwan/Android-Kotlin-Example/tree/master/MyGallery)에서 확인 가능하며 실행화면은 다음과 같다.

![image-20190122105243001](/jekyll/assets/img/2019-02-08-gyuhwan-android-09-01.jpeg)