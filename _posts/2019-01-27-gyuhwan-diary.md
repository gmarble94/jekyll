---
layout: post
title:  "2019년 1월 27일"
date:   2019-01-27 00:00:00 0000
author : "Gyuhwan Kim"
categories: diary
mathjax : true
---

## 안드로이드 공부 (코틀린)

 오늘은 안드로이드 생존코딩 5 ~ 7장에 있는 실습 코드를 작성해볼 예정이다. 각각 BMI 계산기, 스톱워치, 나만의 웹브라우저를 다루고 있다. 작성한 코드는 Github에 public으로 올릴 것이다.

### 5장 BMI 계산기

5장에서는 [**anko 라이브러리**](https://github.com/Kotlin/anko)를 사용하여 좀 더 간결하게 코딩하는 방법과 **Intent**, **SharedPreference**를 이용한 간단한 내용 저장 및 읽어오는 방법에 대해서 다루고 있다. 또한 **ConstraintLayout**에서 뷰 간의 관계를 연결하는 방법에 대해서도 간단하게 설명해주고 있다. 프로젝트 코드는 [**여기**](https://github.com/kimkyuhwan/Android-Kotlin-Example/tree/master/BmiCalculator)에서 확인 가능하며 실행화면은 다음과 같다.

![image-20190122105243001](/jekyll/assets/img/2019-01-27-gyuhwan-android-05-01.jpeg)

![image-20190122105243001](/jekyll/assets/img/2019-01-27-gyuhwan-android-05-02.jpeg)



### 6장 스톱워치

6장에서는 [**Design 라이브러리**](https://guides.codepath.com/android/Design-Support-Library)를 사용하여 **FloatingButton**을 사용하는 방법과 **timer**를 사용하여 백그라운드에서 일정 시간 간격으로 코드를 백그라운드에서 실행하는 방법에 대해서 배우고, **ScrollView**의 사용법에 대해서 설명해주고 있다. 또한 **동적으로 레이아웃에 뷰를 추가**하는 방법에 대해서도 다룬다. 그리고 **벡터 이미지를 아이콘으로 설정**하여, 배경과 콘텐츠 색을 설정하는 방법에 대해서도 알려주고 있다. 프로젝트 코드는 [**여기**](https://github.com/kimkyuhwan/Android-Kotlin-Example/tree/master/StopWatch)에서 확인 가능하며 실행화면은 다음과 같다.

![image-20190122105243001](/jekyll/assets/img/2019-01-27-gyuhwan-android-06-01.jpeg)

### 7장 나만의 웹 브라우저

7장에서는 [**anko 라이브러리**](https://github.com/Kotlin/anko)를 사용하여 **암시적 인텐트**를 간결하게 사용하는 방법과 앱 권한을 설정하는 방법에 대해서 알려주고 있다. 그리고 **WebView**와 **옵션 메뉴**, **컨텍스트 메뉴**, 그리고 **테마**를 수정하는 방법에 대해서도 다루고 있다. 프로젝트 코드는 [**여기**](https://github.com/kimkyuhwan/Android-Kotlin-Example/tree/master/MyWebBrowser)에서 확인 가능하며 실행화면은 다음과 같다. 

![image-20190122105243001](/jekyll/assets/img/2019-01-27-gyuhwan-android-07-01.jpeg)