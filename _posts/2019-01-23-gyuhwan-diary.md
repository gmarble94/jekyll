---
layout: post
title:  "2019년 1월 23일"
author : "Gyuhwan Kim"
date:   2019-01-23 12:00:00 -0300
categories: diary
mathjax : true
---
## 디지털 신호 처리 (DSP) 2일차 (Ch. 2)

### 단어 정리

collection : 수집품, 무리, 더미

radian frequency : 각진동수

trigonometric function : 삼각함수

### 강의 내용

Sinusoidal signals는 cosine signals 또는 sine  signals로 이루어져 있다. 이를 공식으로 표현하면 다음과 같다.

\\[
x(t)=Acos(\omega_0+\theta)
\\]

위의 식에서 $$A​$$ 는 진폭을 의미하는 상수이다.$$\omega_0​$$ 는 각진동수를 의미하는 상수이다.$$\theta​$$ 는 phase shift를 의미하는 상수이다.

예를 들면,
\\[
x(t) = 10cos(2\pi(440)t-0.4\pi)
\\]
라는 식이 있다면, 다음 그림과 같은 모양을 띄게 된다.

![image-20190122105243001](/jekyll/assets/img/2019-01-23-01.png)



진폭이 10이므로 $$x(t)$$값은 -10에서 10 사이의 값을 갖게되고, 각진동수는 
\\[
2\pi(440)
\\]
이므로 주기는 1/440이 된다. 1000/440 = 2.27272727..이므로 약 2.27 msec가 주기가 된다.
\\[
x(t)=10cos(\pi(880t-0.4))
\\]
이라고 표현할 수 있으며, $$t$$값이 0.4/880 sec일 때 $$x(t)=10$$ 값을 갖게 된다. 이 때, $$t$$는 약 0.45msec 이다.

sine함수는 삼각함수로 표현하면
\\[
sin\theta=\frac y r ,y=rsin\theta
\\]
로 표현할 수 있고, cosine함수는 삼각함수로 표현하면
\\[
cos\theta=\frac x r, x=rcos\theta
\\]
로 표현할 수 있다.

Sine은 각 theta에 대하여 아래 그림과 같은 파형을 띈다.

![image-20190122105243001](/jekyll/assets/img/2019-01-23-02.png)

Cosine은 각 theta에 대하여 아래 그림과 같은 파형을 띈다. 

![image-20190122105243001](/jekyll/assets/img/2019-01-23-03.png)

그리고 삼각함수는 다음과 같은 기본 성질을 갖는다.

1) 
\\[
sin\theta=cos(\theta-\frac \pi 2)
\\]
2)
\\[
sin\theta=cos(\theta-\frac \pi 2) \\ or \\ cos(\theta)=sin(\theta+\frac \pi 2)
\\]
3)
\\[
cos(\theta+2\pi k)=cos\theta, \ when \ \ k \ \ is \ \ integer
\\]
4)
\\[
cos(-\theta)=cos\theta
\\]
5)
\\[
sin(-\theta)=-sin\theta
\\]
6)
\\[
sin(\pi k)=0, \ when \ \ k \ \ is \ \ integer 
\\]
7) 
\\[
cos2\pi k =1, \ when \ \ k \ \ is \ \ integer 
\\]
8)
\\[
cos[2\pi(k+\frac 1 2)]=-1, \ when \ \ k \ \ is \ \ integer 
\\]
그리고 기본 성질 두 번째는 

1)
\\[
sin^2\theta+cos^2\theta=1
\\]
2)
\\[
cos2\theta=cos^2\theta-sin^2\theta
\\]
3) 
\\[
sin(2\theta)=2sin\theta cos\theta
\\]
4)
\\[
sin(\alpha\pm\beta)=sin\alpha cos\beta \pm cos\alpha sin\beta
\\]
5)
\\[
cos(\alpha\pm\beta)=cos\alpha cos\beta \pm sin\alpha sin\beta
\\]
이다.

#### Equivalence Properties

\\[
x(t) = Acos(\omega_0t+\phi) =Acos(2\pi f_0t+\phi)
\\]

진폭은 $$[-A, A]$$이고, 각진동수는 $$\omega_0$$, $$f_0$$는 cyclic frequency이다. 위의 식에서 아래와 같이 가정하면
\\[
\phi =\phi'-\frac \pi 2
\\]
아래와 같이 변형할 수 있다.
\\[
x(t)=Asin(\omega_0t+\phi)=Acos(\omega_0t+\phi'-\frac \pi 2)
\\]
예를 들면
\\[
x(t)=20cos(2\pi(40)t-0.4\pi)
\\]

\\[
A=20,\omega_0=2\pi(40),f_0=40,\phi=-0.4\pi
\\]

일 때, 최대 값은 $$t$$ = ... , -0.02, 0.005, 0.03, … 일 때, 최소 값은 $$t$$ = ..., -0.0325, -0.0075, 0.0175, … 일 때이다.

값들이 0.025초의 차이를 갖는 것을 볼 수 있으며, 이는 
\\[
\frac 1 f_0 = 0.025  sec
\\]
주기가 0.025초이기 때문이다.

#### 진동수와 주기의 관계

주기는 sinusoid의 한 사이클의 길이를 의미한다.
\\[
x(t+T_0)=x(t) \\
Acos(\omega_0(t+T_0)+\phi)=Acos(\omega_0t+\phi) \\
cos(\omega_0t+\omega_0T_0+\phi)=cos(\omega_0t+\phi)
\\]
cosine함수의 주기는 2$$\pi$$이므로, 주기는 다음과 같이 쓸 수 있다.
\\[
\omega_0T_0=2\pi ,\quad T_0=\frac {2\pi} {\omega_0} \\
(2\pi f_0)=2\pi  ,\quad T_0=\frac 1 {f_0}
\\]
즉, 주기는 1 / 진동수와 같다. 따라서, 진동수가 커지면 주기가 짧아지고, 진동수가 작아지면 주기가 길어진다. 예를 들면 다음 그림과 같다.

![image-20190122105243001](/jekyll/assets/img/2019-01-23-04.png)

#### Time Shifting

$$s(t)$$라는 signal이 있다고 하자. 이 때, phase shift값으로 $$a$$값을 주면 $$s(t + a)$$가 되고, 이는 signal을 $$-a$$만큼 이동한 것이다. 예를 들어 $$a=2$$이라면 $$t=0$$ 일 때 $$s(0 + 2) = s(2)$$값이 된다. 따라서 0초 일때 원래 signal의 $$t = 2$$ 일때 값을 갖으므로 원래 signal에서 왼쪽으로 2초만큼 이동한 signal로 변형된다.

#### 샘플링

실제 신호를 디지털 신호로 바꾸는 작업을 샘플링이라고 한다. 샘플링은 주기에 따라 원본을 나타내는 정도가 달라진다. 주기를 길게 할 경우 원본은 제대로 나타내지 못하고, 주기를 짧게 할 경우 원본을 비교적 제대로 나타낼 수 있게 된다. 예를 들면 다음과 같다.  

![image-20190122105243001](/jekyll/assets/img/2019-01-23-05.png)

#### 복소수

복소수는 $$z = x + jy$$ 와 같은 꼴로 표현할 수 있다. 여기서 $$x$$는 실수를 표현하는 부분이고, $$y$$는 허수를 표현하는 부분이다. 그리고 극값으로 표현하면 
\\[
z=re^{j\theta}
\\]
와 같은 방법으로도 표현할 수 있으며 이 두 방법을 그림으로 나타내면 다음과 같다.

![image-20190122105243001](/jekyll/assets/img/2019-01-23-06.png)

$$a$$를 Cartesian coordinate라고 하고, $$b$$를 Polar form이라고 한다. 각 표현방법은 다른 표현방법으로 변환이 가능하다.

먼저, $$a$$를 $$b$$로 변환하는 방법이다.
\\[
x=rcos\theta \quad and \quad y=rsin\theta
\\]
다음은 $$b$$를 $$a$$로 변환하는 방법이다.
\\[
r=\sqrt{x^2+y^2} \quad and \quad \theta=arctan(\frac y x)
\\]
다음은 오일러 공식을 이용하여 $$b$$를 표현하는 방법이다.
\\[
e^{j\theta}=cos\theta+jsin\theta
\\]
다음은 오일러 공식을 이용하여 $$a$$를 표현하는 방법이다.
\\[
z=re^{j\theta}=rcos\theta+jrsin\theta
\\]
다음은 complex exponential signal을 일반적인 형태로 나타낸 것이다.
\\[
z(t)=Ae^{j(\omega_0t+\phi)}\\=Acos(\omega_0t+\phi)+jAsin(\omega_0t+\phi)
\\]

\\[
z(t)의 \, 진폭은 \, |z(t)|=A이며 \\
z(t)의 \, 각은 \, \omega_0t+\phi이다.
\\]

예를 들면 다음과 같다.

![image-20190122105243001](/jekyll/assets/img/2019-01-23-07.png)

#### 복소수의 곱셈

$$z_3$$을 $$z_1$$과 $$z_2$$의 곱셈이라고 한다면 다음과 같이 나타낼 수 있다.
\\[
z_3=z_1z_2, \quad z_1=r_1e^{j\theta_1} \quad and \quad z_2=r_2e^{j\theta_2} \quad and \\
z_3=r_1e^{j\theta_1}r_2e^{j\theta_2}=r_1r_2e^{j\theta_1}e^{j\theta_2}=r_1r_2e^{j(\theta_1+\theta_2)}
\\]
즉, $$z_3$$의 각은 $$z_1$$의 각과 $$z_2$$의 각을 더한 값이다. ![image-20190122105243001](/jekyll/assets/img/2019-01-23-08.png)

#### 복소수의 덧셈

$$z_3$$의 $$z_1$$과 $$z_2$$의 덧셈이라고 한다면 다음과 같이 나타낼 수 있다.
\\[
z_1=x_1+jy_1 \\
z_2=x_2+jy_2 \\
z_3=z_1+z_2=(x_1+x_2)+j(y_1+y_2)
\\]
![image-20190122105243001](/jekyll/assets/img/2019-01-23-09.png)

#### rotaing phasor

\\[
|e^{j\omega_0t}|=1 \\
if \ \omega_0 \  is \  positive \quad rotate \ to \ the \ countclockwise \\
if \ \omega_0 \  is \  negative \quad rotate \ to \ the \ clockwise
\\]

#### inverse Euler Formulas

\\[
cos\theta=\frac {e^{j\theta}+e^{-j\theta}} 2 \\
sin\theta=\frac {e^{j\theta}-e^{-j\theta}} {2j}
\\]

## 점심식사

점심식사는 가메이에 가서 김치치즈가쯔동을 먹었다 (7000원)

![image-20190122105243001](/jekyll/assets/img/2019-01-23-lunch.jpeg)

점심식사를 마치고 도윤이형과 함께 카페로 갔다.

## 카페

어제는 Cafe Ho에서 체리초코를 먹었다(밤에 잠을 못잘까봐). 오늘은 따뜻한 아메리카노를 시켜서 먹었다.

![image-20190122105243001](/jekyll/assets/img/2019-01-23-cafe.jpeg)

## 소프트웨어 공학 개론 5 ~ 7강 정리

### 5장 사용사례 [23분 56초]

사용 사례(Use Case)란 기능적인 요구사항을 사용자와 개발자가 결정하여 표현한 것이다.  기능을 명확하고 일관성있게 표현하여 의사소통의 수단으로 활용한다. 개발된 시스템을 초기의 요구사항과 비교하여 제대로 만들어졌는지 검증하는 용도로 사용한다.  

1) 시스템 사용의 사례로서 시스템의 사례들을 그려놓은 것

2) 시스템의 외부에서 본 기능

3) 개발자와 사용자간의 상호작용을 표시한 것

4) 목적은 시스템의 기능을 정의하는 것

#### 사용 사례 다이어그램

사용 사례 다이어그램은 다음과 같이 표현한다.

![image-20190122105243001](/jekyll/assets/img/2019-01-23-11.png)

#### 사용 사례 기술

주요 성공 시나리오, 확장(오류 시나리오), 변형(선택 시나리오) 등으로 표현한다.

본 강의는 workbook을 통해 주어진 요구사항에 맞게 사용 사례 다이어그램과 사용 사례 시나리오를 만들어보는 시간을 주었다.

### 6장 클래스 다이어그램 [18분 47초]

클래스 다이어그램에는 클래스, 어트리뷰트, 클래스 간의 관계(상속, 연관, 집합) 등이 담겨있다.

클래스 다이어그램은 클래스 내부의 정적인 내용이나 클래스 사이의 관계를 표기하는 다이어그램으로 시스템의 일부 또는 전체의 구조를 나타낼수 있다. 클래스 다이어그램은 클래스들의 관계를 쉽게 보고, 의존관계를 쉽게 파악하게 해준다.

본 강의는 workbook을 통해 주어진 요구사항에 맞게 클래스 다이어그램을 만들어보는 시간을 주었다.

### 7장 시퀀스 다이어그램 [28분 11초]

시퀀스 다이어그램은 문제 해결을 위한 객체를 정의하고 객체간의 상호작용 메시지 시퀀스를 시간의 흐름에 따라 나타내는 다이어그램이다.

본 강의는 workbook을 통해 주어진 요구사항에 맞게 시퀀스 다이어그램을 만들어보는 시간을 주었다.

> 오늘 강의는 전체적으로 본 강의를 현장에서 수강하는 사람들 위주로 이루어져 있어서 아쉬웠다.

## 저녁식사

저녁식사는 노진이 집에서 생일 때 받은 쿠폰으로 BHC 치킨을 시켜먹었다. ( 후라이드 치킨과 콜라 1.25L )

![image-20190122105243001](/jekyll/assets/img/2019-01-23-dinner.jpeg)



## To-Do List

오늘부터 포스팅 마지막 부분에 내일 할 일이나 다음 시간에 할 일을 **To-Do List**로 적어서 관리할 예정이다.

- 디지털 신호처리 3강
- **논문 읽고 related Work 작성 (중요) - 자유시간에도 해야할 듯** (8시간 정도 소요 예상)
- 소프트웨어 공학 개론 (8~9강)
