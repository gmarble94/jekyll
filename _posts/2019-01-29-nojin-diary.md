---
layout: post 
title: "2019년 1월 28일"
date: 2019-01-29 00:00:00 0000 
author : "Rojin Jung" 
categories: education 
mathjax : true
---

## 11시 30분 기상 -> 겁나 늦게 일어남...;; 너무 나태해져있다

11시 30분 ~ 11시 50분 씻기;

11시 50분 ~ 12시 30분 점심먹고 설거지

12시 30분    1시: 휴식

1시~6시 codeforces 문제

## 다시 풀 문제 

http://codeforces.com/contest/1060/problem/E



## 문제풀이

http://codeforces.com/problemset/problem/208/B

60* 60* 60 * 60dp로 각자 top에 뭐가 있는지를 저장하면서 몇번을 최대로 옮길 수 있는지 확인을 하고, 최대값이 n-1이 되면 yes 아니면 no



http://codeforces.com/problemset/problem/38/E

처음에 그리디로 접근해서 오른쪽에서 부터 차례대로 굴릴지 말지를 정해서 풀었다가 fail

->안되는 이유 지금 당장은 멈추는게 이득이라고 볼 수 있지만 다음 차례때의 이득과 다를 수 있음

->그 후 그냥 n^2로 접근해서 쉽게품 -> 풀이 자체는 쉬운데 잘못생각해서 틀림



http://codeforces.com/problemset/problem/30/C

이것도 n^2 dp문제인데 문제에서 물어본게 맨하탄 거린줄 알고 구했다가 계속 fail

dd한테 물어봐서 피타고라스로 거리를 구하는 문제여서 다시풀고 accept 

->영어 너무못함;;;



http://codeforces.com/problemset/problem/518/D

n*t dp문제-> 그냥 간단



http://codeforces.com/problemset/problem/518/D

dfs문제 쉽



6시~7시 저녁

7시~9시 30분 atcoder 90 round

http://codeforces.com/problemset/problem/518/D

그냥 300점짜리 쉽

https://atcoder.jp/contests/arc090/tasks/arc090_b

그냥 이것도 dfs 400점짜리 쉽

https://atcoder.jp/contests/arc090/tasks/arc090_c

다익스트라 때리고 dfs돌리고 위상정렬 때리고 별짓다해서 결국 맞추긴맞췄는데;;;; 

다익 2번돌려서 간선만보면됬던문제-> 난이도는 어느정도 있었는데 단계별로 잘 풀어나가면 되는문제(700점)-> 오랜만에 700점짜리 풀었다.



10시~12시

atcoder 79 round

https://atcoder.jp/contests/arc079/tasks/arc079_a

그냥 쉽(300점)

https://atcoder.jp/contests/arc079/tasks/arc079_a

n을 50잡고 규칙찾아서 풀면되는건 알았는데 너무 오래걸렸다.(600점)

-> 이런 문제에 너무 약함;;;

https://atcoder.jp/contests/arc079/tasks/arc079_c

그냥 수식세워서 돌리면 되는 문제(600점)