---
layout: post
title:  "2019년 1월 28일"
date:   2019-01-28 00:00:00 0000
author : "Gyuhwan Kim"
categories: diary
mathjax : true
---



## 소프트웨어 공학 개론

### 8장 설계원리 [61분 24초]

요구 분석 작업을 통하여 무엇을 개발할 것인가를 결정한 다음 도메인 영역에 집중하여 **모델링**한다. (도메인은 실제로 어떻게 구성할 건지 However) 

설계란 찾은 요구를 어떻게 구현할 것인가를 고민하는 단계이며 4가지 종류가 있다.

1) 아키텍처 설계 : 소프트웨어 내부 구조 설계, 아키텍처는 상위 수준의 설계로서 시스템의 컴포넌트간의 관계를 초점에 둔 설계

2) 모듈 설계 : 모듈 안의 자료구조와 알고리즘을 설계한다.

3) UI 설계 : 유저 화면 설계

4) 데이터 설계 : 모델, 애플리케이션이 공유하는 자료 설계

#### 아키텍처 설계

소프트웨어 아키텍처란 주요 컴포넌트 사이의 인터페이스와 인터렉션을 포함한 시스템 구조의 설계 유형을 말한다. 아키텍처 설계는 개발 중인 시스템에 대한 아키텍처를 정하는 의사 결정 과정이다.

#### 컴포넌트

명백한 역할의 가지고 있으며 독립적으로 존재할 수 있는 시스템의 부분을 말한다.

#### 모듈

모듈이란 프로그래밍 언어 문법 구조를 통해 정의된 컴포넌트이다. 예를 들면, Java의 모듈은 소드, 클래스, 패키지이다. 그리고 C언에서 모듈은 함수이다.

#### 좋은 설계

 복잡한 문제를 단순한 조각으로 나누는 것이며  복잡한 문제를 해결하는 방법은 복잡한 예외처리를 만드는 것이 아니라 간결하게 작성하는 것이다. 바람직한 설계의 목표에는 7가지가 있다.

1) 복잡성 최소화  

2) **느슨한 결합**  : 두 객체가 느슨하게 결합되어 있다는 것은, 그 둘이 상호작용을 하긴 하지만 서로에 대해서 서로 잘 모른다는 것을 의미합니다.

3) **강한 응집력** : 응집도란 프로그램의 한 요소가 해당 기능을 수행하기 위해 얼마만큼의 연관된 책임과 아이디어가 뭉쳐있는지 나타내는 정도이다. 따라서 프로그램의 한 요소가 특정 목적을 위해 연관된 기능들이 구현되어 있고, 지나치게 많은 일을 하지 않으면 응집도가 높다고 한다. 이렇듯 응집도가 높으면 이해하기 쉽고 재사용과 유지보수가 쉽다는 장점이 있다.  

4) 확장성  

5) 재사용성  

6) 유지 보수성  

7) 유연성  

## 디지털 신호 처리 (DSP) 3일차 (Ch. 3) Spectrum Representation

### 선형 시스템

선형 시스템에는 **1) additivity property**와  **2) scaling property**가 적용된다. 먼저 1)을 수식으로 나타내면 다음과 같다.
\\[
T \\{ x_1[n]+x_2[n] \\} =T \\{ x_1[n] \\} +T \\{ {x_2[n] \\} =y_1[n]+y_2[n]}
\\]
다음은 2)를 수식으로 나타낸 것이다.
\\[
T \\{ ax[n] \\} =aT \\{ x[n] \\} =ay[n]
\\]
1), 2) 두 가지를 혼합하면 다음과 같이 쓸 수 있다.
\\[
T \\{ ax_1[n]+bx_2[n] \\} =aT \\{ x[n] \\} +bT \\{ x_2[n] \\} =ay_1[n]+by_2[n]
\\]
입력 시퀀스에서 time shift나 delay가 일어날 경우 출력 시퀀스에도 똑같이 적용한다.
\\[
x_1[n]=x[n-n_0]=>y_1[n]=y[n-n_0]
\\]

### 정현파(Sum of Sinusoids)의 스펙트럼

임의의 신호는 여러가지 진폭, 위상 및 주파수가 다른 Sinosoids 신호의 합으로 표현이 가능하다.
\\[
x(t) = A_0 + \sum_{k=1}^NA_kcos(2\pi f_kt+\phi_k) \\
=X_0+\kappa e \\{ \sum_{k=1}^NX_ke^{j2\pi f_kt} \\}  \\
A_0=X_0:real \, \,constant \\
X_k=A_ke^{j\phi k} (phaser)
\\]
Inverse Euler formula를 사용하면 다음과 같은 형태로도 x(t)를 표현할 수 있다.
\\[
x(t)=X_0+\sum^N_{k=1} \\{ \frac {X_k} 2 e^{j2\pi f_kt}+\frac {X^*_k} 2 e^{-j2\pi f_kt} \\} 
\\]

### 양면 스펙트럼

각각의 sinusoid는 두 개의 rotation phasor로 분해된다. 하나는 양수 주파수를 갖고, 다른 하나는 음수 주파수를 갖는다.

## 영어 단어

summation : 요약

decompose : 분해하다

