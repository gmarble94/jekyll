---
layout: post
title:  "mathjax 수정 for CTD"
date:   2019-02-09 00:00:00 0000
author : "Gyuhwan Kim"
categories: education
---

## mathjax 수정

$a$

```wiki
$a$이라고 입력할 경우 위와 같이 나오게 됩니다.(변수를 수식 기호로 표현할 경우 사용)
```

$$A$$

```wiki
$$A$$이라고 입력할 경우 위와 같이 가운데 정렬되어 나오게 됩니다. (수식 설명시 사용)
```

수식을 사용하고 싶으신 분들은 위 내용을 참고하여 사용하시면 됩니다.

#### _layout/default.html

해당 레이아웃에 body에 다음의 내용을 추가하였습니다.

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML"></script>
<script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [ [ '$', '$' ] ],
            processEscapes: true
        }
    });
</script>
```

