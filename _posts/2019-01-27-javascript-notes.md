---
layout: post
title: "JavaScript notes"
date: 2019-01-27
author: "Hakkyu Kim"
categories: Programming Language
mathjax: true
---

## Javascript

Details of programming language

Object, property, method

function, argument

prototype

constructor, destructor



Programming Fundamentals.

Programming language is just a tool.

abstracting away from how the computer works and how the browser works to implement javascript or the server that's running the javascript.

We need to know how the tool is functioning.



## Fundamental Concepts and Ideas

### Syntax Parsers

A program that reads my code and determines what it does and if its grammar(syntax) is valid.

One of the job that compiler(Interpreter) does.

The "Engine" that is translating my code can choose to do extra stuff.

![Syntax Parsers](/jekyll\assets\img\Syntax Parsers.png)

### Lexical Environments

Where the code sits physically in JavaScript file.

Where the variable is written physically and what surrounds it. (i.e. Scope)



### Execution Contexts

> excuting section of the code

A wrapper to help manage the code that is running.



It will give us the basis of understanding javascript deeply.



### NAME/VALUE PAIRS AND OBJECT

The name may be defined more than once, but only can have one value in any given __context__.

Object(in javascript): A collection of name value pairs



### The Global Environment and The Global Object

JS Engine will wrap my executing code in an execution context.

Execution Context(Global)

- Global Object(Browser: window object)
- this

Global(in javascript): Not inside a function

If variables and functions are declared outside of a function, it is attached to the global object.

![execution context](/jekyll\assets\img\execution context.png)

### Execution Context Creation and Hoisting

```javascript
console.log(<String>)
            // Write the devtool window <String>
            // Execute(call, invoke) a function
```



```javascript
b()
console.log(a)

var a = "Hello World!"; // a is defined

function b(){ // function b is defined
    console.log("Called b!");
}

/*Hoisting
Called b!
undefined
*/
```

Okay...but why is it important?

The reason javasciprt behaves the way it does; the variables and functions are available to some degree even though they were written in the later in the code, is because the execution context is created in two phases.

#### Phase 1. Creation Phase

Global Object and 'this' are set-up in memory. (Gloabl Object is only dealt when global execution context is created)

At this phase, the syntax parser goes through my entire code and setup __Memory Space__ for __Variables__ and __Functions__(__Hoisting__).

Functions are setup in memory as a whole.

setup placeholders _undefined_(special value which means that the variable has not been set yet) to all variables initially.

Different from ("I don't have it in memory")

![javascript not defined error](/jekyll\assets\img\javascript not defined error.png)

#### Phase 2. Code Execution Phase

Runs my code line by line. Interpreting, Converting, Compiling, Executing it



### Single Threaded

One command at a time

behaves in a single threaded manner



### Synchronous

__One at a time__ and in order

(Vanilla) Javascript is syncrhonous execution in behavior



### Function Invocation and the Execution Stack

When a function in invoked, a new execution context is created and put on the __Execution Stack__.

The __top__ execution context is always running. (Create Phase -> Execute Phase)



### Variable Environment and Outer Environment

```javascript
function a(){
    var myVar;
}

function b(){
    var myVar = 2;
    a();
}

myVar = 1;
b();
```

In the first phase of creating an execution stack, the variables are put in its execution stack _variable environment_. This decides where the variable lives, the world that the variable lives is the variable environment. This concepts relates to the _scope_ of a variable, but the word scope can be misleading.

```javascript
function a(){
    console.log(myVar);
}

function b(){
    var myVar = 2;
    console.log(myVar);
    a();
}

myVar = 1;
console.log(myVar);
b();

```

The output of this code is

```
1
2
1
```

We might ask why the _console.log(myVar)_ in function a() printed '1' and not 'undefined'. This is because execution context has an outer environment reference that can access variables of other execution context. Then why did it reference the global execution context? This reference depends on where the code sits lexically. Its lexical environment is global. 

```javascript
function b(){
    var myVar = 2;
    function a(){
        console.log(myVar);
    }
    console.log(myVar);
    a();
}

myVar = 1;
console.log(myVar);
b();

```

Hence, this code generates

```
1
2
2
```

The chain of outer environments is called _Scope Chain_.



### Blocking scope

```javascript
if(a > b){
    let c = true;
}
```

Variables declared in _if_ block and _for_ block can only be assessed when the execution is in that block, variables are still in memory, but the engine won't allow to access them otherwise.



### What about asynchronous callbacks?

Asynchronous: More than one at a time

What only happens in the JavaScript Engine is run synchronously.

There is a component called _Event Queue_. This is similar to interrupt. We can make functions that handles some types of events. When the JavaScript Engine's execution stack is empty, it periodically looks the event queue and handles it by calling _handler functions_.

Hence, the JavaScript engine is always synchronous. The browser asynchronously puts events inside the event queue so the browser seemingly gets asynchronous callbacks.

```javascript
function waitThreeSeconds(){
    var ms = 3000 + new Date().getTime();
    while(new Date() < ms){};
    console.log("finished function");
}

function clickHandler(){
    console.log("click!");
}

document.addEventListener('click', clickHandler);

waitThreeSeconds();
console.log("finished execution")
```

The output is

```
finished function
finished execution
click!
```

So, a long function can block events.



### Types

JavaScript is a _dynamic typing_ language. (The opposite is _static typing_)

__Primitive types__(6): Not an object and single value

undefined, null, boolean, number, string, symbol(ES6)



### Operator

operator: a special function that is syntactically written differently, usually they take two parameters and return one result. (Infix notation of a function.)

__precedence__ and __associativity__(left to right, right to left when they have same precedence)



### Coercion

Converting a value from one type to another(Casting)



### Comparison operators

It can cause weird effects in JavaScript. 

For example: <kbd>==</kbd> and <kbd>===</kbd>



### Setting default value

```javascript
funciton a(name){
    name = name || "<Your name here>";
    console.log("Hello " + name);
}
```



### Framework

```html
<script src="lib1.js"></script>
<script src="lib2.js"></script>
<script src="app.js"></script>
```

The JavaScript source code is just added from top to bottom.



### Objects and functions

Object can have _primitive property_, _object property_, _function method_.

The object in memory have references to properties and methods in memory.

```javascript
var person = {} // Object literal
person.name = "Tony" // primitive property string is made and referenced by person Object
```

Usually syntax doesn't matter to the JavaScript Engine, under the hood, it is doing the same thing. So using certain syntax is for clarity for humans.



__namespace__: a container for variables and functions

But JavaScript don't have name spaces, but we can fake it with objects.



### JSON and object literal

Sending data across the Internet changed to XML to JSON because XML wastes a lot of bandwidth by sending necessary characters.

A valid JSON needs double quotes around property names and can't have functions as values. It is similar to JavaScript object literal but not the same. There are handy built in methods that can convert one another.

```javascript
JSON.stringify() // js object to string JSON
JSON.parse() // String of JSON notation to jS object
```



### Functions are object

__First class functions__: Everything you can do with other types(primitive types), you can do with functions.

And why is this important? This is important because having this property changes the way you code. (i.e. Assigning functions to variables, passing them around as arguments(__functional programming__), creating them on the fly)



Function object have hidden special properties.

__Name__ and __Code__(Code are __invocable__(), this property contains the code I wrote)

Function objects are invocable by using parenthesis. 



__Expression__: A unit of code that results in a value.

__Statement__: No returns, it just does work.



In JavaScript , there are function statements and function expressions.

```javascript
function greet(){ // function statement
    console.log("hi");
}

var anonymousGreet = function(){ // function expression
    console.log("hi");
}
```

```javascript
anonymousGreet();

var anonymousGreet = function(){ // when this code is executed, the javascript will run the expression and create a function object.
    console.log("hi");
}
```

The above code will generate an error.



### By value and By Reference

picture by value

picture by refernce

```javascript
var c = {greetings: 'hi'};
var d = c;

c.greetings = 'hello';

console.log(c.greetings); // hello
console.log(d.greetings); // hello

c = {greetings: 'howdy'};
console.log(c.greetings); // howdy
console.log(d.greetings); // hello

```



### this

When code is run, (function is invoked) execution context is generated. _this_ will be set to different objects depending on different situations. _this_ is decided by the which object runs the code. But just by one level...this can be very confusing.

```javascript
var c = {
    name = 'The c object',
    log: function(){
        var that = this;
        
        that.name = 'Updated c object';
        
        var setname = function(newname){
            that.name = newname;
        }
        setname('Updated again!');
        console.log(name))
    }
}
```



### Array

```javascript
var arr = [];
```

Similar to python arrays, the elements in a single array can have different types.

__arguments keyword__

Arraylike object(similar to array but missing some features) is created. 

Keyword arguments will be gradually deprecated.



### Function Overloading

The method does not exist in JavaScript, the language has other patterns to implement similar concept.



__Automatic semicolon insertion__ trait provided by the JavaScript Engine is a dangerous feature. You should always explicitly writes semicolon.



### arguments and spread

The execution context generates arguments.



### Immediately invoked function expressions(IIFEs)

```javascript
var greeting = function(name){
    console.log('Hello' + name);
}('John');

console.log(greeting) // Hello John
console.log(greeting()) // This will now generate an error
```

```javascript
// We need to close the code with parenthesis to let the engine know that it is an expression.
(function(name) {
    console.log("hello!");
}());
```



### Closures

Closures are features of JavaScript that we take advantage of. The engines makes sure that an execution context of a function have proper scope chaining even though the reference to that execution context should not exists.

```javascript
function buildFunctions(){
    var arr = [];

    for(var i =0; i< 3; ++i){
        arr.push(
            function(){
                console.log(i);
            }
        )
    }
    return arr;
}

var fs = buildFunctions();

fs[0](); // 3
fs[1](); // 3
fs[2](); // 3 WOW!
```



### Function factories

Using closures to our advantages.

I can create functions using a factory function.

pic add



### Closure and callbacks

```javascript
// This code uses closures
fuction sayHiLater(){
    var greeting = 'Hi';
    
    setTimeout(function(){
        consol.log(greeting);
    }, 3000); // browser triggers event after 3 secs. 
}

sayHiLater();
```

__callback function__: a function you give to another function, to be run when the other function is finished.

```javascript
function tellMeWhenDone(callback){
    var a = 1000;
    var b = 2000;
    callback();
}

tellMeWhenDone(function(){
    console.log("I am done!");
})

tellMeWhenDone(function(){
    alert("I am done!");
})
```



### call(), apply(), bind()

A function has three special methods; call(), apply(), bind().

All of these functions are related to the keyword _this_.

These functions are used in _function borrowing_, _function currying_.

__function curring__: creating a copy of a function but with some preset parameters.



### Functional programming

- fine granularity of functions

```javascript
function mapForEach(arr, fn){
    var newArr = [];
    for(var i = 0; i < arr.length; ++i){
        newArr.push(
            fn(arr[i])
        );
    }
    return newArr;
}

arr = [1, 2, 3]
var arr3 = mapForEach(arr, function(item){return 2*item});
console.log(arr3) //[2, 4, 6]

var checkPastLimit = function(limiter, item){
    return item > limiter;
}

var arr4 = mapForEach(arr, checkPastLimit.bind(this, 1));
console.log(arr4);

var checkPastLimitSimplified = function(limiter){
    return function(limiter, item){
        return item > limiter;
    }.bind(this, limiter);
}

var arr5 = mapForEach(arr, checkPastLimitSimplified(1));
console.log(arr5);
```



#### Object-Oriented JavaScript and Inheritance

Inheritance: one object gets access  to the properties and methods of another object.

Classical vs __Prototypal Inheritance__: different method of implementing the idea of inheritance.

Every object in JavaScript has a hidden property called _proto_.

![prototype chain](/jekyll\assets\img\prototype chain.png)

#### Prototype Chaining

JavaScript objects can access variables and methods of its prototype object. Because this rule is recursive,  we call it prototype chaining.

```javascript
// Remember, Object is a default function constructor for object literals.
// Object.prototype is the prototype of all JavaScript objects
// An object have reference to its prototype object, in __proto__ property. But we should not directly access this property in practice.

function Dog(name) {
  this.name = name;
}

let beagle = new Dog("Snoopy");

Dog.prototype.isPrototypeOf(beagle);  // => true

// Fix the code below so that it evaluates to true
Object.prototype.isPrototypeOf(Dog.prototype);

```





#### Reflection and Extend

JavaScript can look at itself, we can use this property to implement something useful

The concept of "extend" is not a built in function, but we several libraries implement it in someway.

It is different from "extends"



#### Building Objects

__Function constructors__ and keyword new.

: functions that construct objects used together with keyword "new".

This was made to attract JAVA developers.

JavaScript don't have class.

```javascript
function Person(){
    this.firstname = "John";
    this.lastname = "Doe";
}

var john = new Person(); // new keyword is actually an operator
console.log(john);
```

Keyword "new" immediately creates a new object. and runs the function "Person()". The "this" keyword in the function is linked to the empty object just created by the keyword "new". If the function after keyword "new" does not return any objects, it will return the object just created.



#### Setting prototypes with function constructors

Every function has a special property called "prototype". (This is not actually the prototype of this function object. Remember, that is accessed by \_\_proto\_\_)

This property is used to make the prototype of the object when the function is used as a function constructor.

Good code example. Every object takes memory space.

```javascript
function Person(firstname, lastname){
    this.firstname = firstname;
    this.lastname = lastname;
}

Person.prototype.getFullName= function(){
    return this.firstname + " " + this.lastname;
}

var john = new Person("john", "Doe");
console.log(john.getFullName());
```

We write function name starting with a capital letter, if the function is intended to use as function constructors.



Although __function constructors create objects__ in JavaScript, __<span style="color:red">they do not become the prototype</span>__ of the created objects, instead __<span style="color:red">the object that the function constructors refer to by the _prototype_ property</span>__ becomes the prototype of the created objects.

![JavaScript Set Prototype](/jekyll\assets\img\JavaScript Set Prototype.JPG)



#### Built-in function constructors

```javascript
var a = new Number(3);
var str = new String("John");
// These are not primitives.
// They look like creating primitives, but actually they are objects.

"John".length
// JavaScript engine automatically wraps up the primitive with the String object that has extra features
// However, JavaScript will not automatically convert number primitives to number objects.
```

```javascript
// If I want to add custom feature to all String objects:
String.prototype.isLengthGreaterThan = function(limit){
    return this.length > limit;
}

console.log("John".isLengthGreaterThan(3));
```



#### Dangerous of using built-in function constructors

```javascript
var b = new Number(3)
var c = new Number(3);

console.log(b == c) // false
```



#### Yet another way to create objects

using protypal inheretence

```javascript
Object.create()
// If the JavaScript Engine is old and doesn't support this new method
// We can do something called polyfill. (Adding code that implements this new functionality)
```



## Server Side JavaScript (NodeJS)

Chrome JavaScript Engine V8 (2008)

NodeJS: A project started by Ryan Dahl from 2009.

NodeJS: V8 + event-driven + non-blocking IO

Runtime environment changes to web browser to server. The usage of language will become different.

For example:

```javascript
alert('Hello world') // alert only works in Web browser
```

![JS Runtime Environment](/jekyll\assets\img\JS Runtime Environment.png)



#### Installation

https://nodejs.org/en

On Windows, after installation, press <kbd>WINDOWS</kbd> + <kbd>R</kbd> and type "cmd".

Type "node --version".



```terminal
node hello.js
```

running js file using nodejs.



#### How does the Internet work - compact summary

keyword: server/client, ip, port

http:///a.com : <span style="color:blue">domain name</span>;human readable address.

Under the hood, the connection is made by the ip address __associated__ with that domain name.



Let's focus on the server-side computer for the moment

A single machine can run many servers(applications). How do we know which application should respond to which request? -> __port__

Some ports are designated for certain server types by convention. ex) port 80: web server

![SingleMachineSeveralServer](/jekyll/assets/img/SingleMachineSeveralServer.png)



#### Modules

NodeJs has collection of modules related to server programming.

```javascript
const http = require('http'); // loads module 'http' to http. 

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World\n');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```



### npn: node package manager

Why do we need this?

Modules like _http_, _os_, ...(Modules offered by NodeJS), _Date_, _Array_, ...(Modules offered by JavaScript Programming Language)

Other module builds can be downloaded and used to create new features, we can even create new modules that abstracts our idea.

Now if we want to use another module not offered by the NodeJS, we need a way to make this work. Npm takes care of this tedious work in an efficient way. (installation, uninstall, upgrade, dependency fixes)

Convention is that package(collection of modules) > module, but not always. Package also covers softwares that can run by itself.

```
npm install uglify-js -g // independent software download
```



directory를 npm의 패키지로 지정을 해야 한다. 왜? underscore라고 하는 다른 사람이 만든 모듈(패키지)를 우리것으로 가지고 오려고 하는데, 우리것도 패키지이기 때문에, 이 디렉토리를 npm 패키지로 지정하는 명령을 먼저 수행해야 한다.

```
npm init
name: (directory name)
version: (xx)
description: (xx)
entry point: (xx.js) // 어떤 js가 이 패키지를 구동하는 js 파일인가?
test command:  // 어떤 커맨드를 입력하면 test code를 실행할 수 있는가?
git repository:  // enter enter enter enter...
...
...
```





# Programming Methodology

man page에서 보이는 [] 이런것들?

Lisp, Haskell?

How is 'object' data structure maintained in programming languages?



### To do

OOP methodology using JavaScript programming language features

