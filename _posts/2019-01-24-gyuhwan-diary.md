---
layout: post
title:  "2019년 1월 24일"
date:   2019-01-24 00:00:00 0000
author : "Gyuhwan Kim"
categories: diary
mathjax : true
---

## Gitlab Page 생성

 어제 작성한 일기를 gitlab에서 보니 수식이 제대로 안들어간다. 이러한 문제를 해결하고, 일기를 작성하고 다음에 확인할 것을 생각해서 Gitlab 저장소에 Page를 만들어서 블로그 형식으로 사용하기로 결정하였다. [Jekyll](https://jekyllrb.com/)를 사용하여 홈페이지를 만들었다. 주소는 [https://gmarble94.gitlab.io/jekyll/](https://gmarble94.gitlab.io/jekyll/)이다. 원래 그룹에 저장소를 만들려고 시도했으나, 자꾸 404에러가 뜨는 바람에 gitlab 아이디에 저장소를 만들었다. 

 기본 값으로 jekyll를 만든 후 post에 기존에 적었던 다이어리를 적었더니 수식이 아예 안보이는 현상이 발생했다. 그래서 수식 사용을 위해서 [mathjax](https://www.mathjax.org/)를 사용하여 문제를 해결하였다. 

또한 Gitlab page는 전체공개 되는 부분이니 기상시간이나 취침시간 등의 정보는 뺄 예정이다.(음식 사진은 올려야지)

**생각보다 Page를 구성하는데 시간이 오래 걸려서 디지털 신호처리와 소프트웨어 공학 개론은 오늘 공부하지 못할 거 같다.. ㅠㅠ** 

## 카페

 오늘도 Cafe HO에 와서 공부를 하려고 한다. 카페에서 논문을 읽을 것이다. 이번에도 역시 따뜻한 아메리카노를 시켰다. 내일은 노진이가 약속이 있어서 내일은 혼자 공부해야 될 거 같고, 연구실 일이 생각보다 많이 남았기 때문에 늦게 자야될 거 같아서 시켰다.. 

![image-20190122105243001](/jekyll/assets/img/2019-01-24-cafe.jpeg)

## 논문

### [An Energy-efficient Protocol for Multimedia Streaming in a Mobile Environment ](https://pdfs.semanticscholar.org/8ecc/d0e4e269389235099296c4bf2499e3ff4746.pdf)

#### Abstract

 만연한 서비스와 스마트한 환경은 많은 사람들이 휴대용 장치를 통해 이러한 서비스를 더욱 더 즐길 수 있도록 만들어 줬습니다. 이러한 장치들은 배터리를 소모하고 따라서 이런 장치들을 사용할 때 에너지 효율성은 매우 중요한 요인이 되었습니다. 이 논문에서는 우리는 모바일 사용자를 대상으로한 멀티미디어 스트리밍 서비스에 초점을 맞출 것입니다. 특히, wi-fi를 사용하여 인터넷에서 스트리밍 서버의 오디오 파일에 접근하는 시나리오를 고려할 것입니다. 우리는 오디오 스트리밍이 실시간 이루어지는 것을 보장하면서, 프록시에 기반한 아키텍쳐와 에너지 효율적인 스트리밍 프로토콜로 모바일 디바이스의 Wi-Fi 인터페이스에서 에너지 소모를 최소화하는 방법에 대하여 제안합니다. 프로토 타입 구현으로 수행된 실험 결과에 따르면 해당 솔루션은 총 에너지 소비량의 76 ~ 91%의 에너지를 절약할 수 있습니다. 또한 오디오 품질도 사용자 수준의 좋은 품질로 제공할 수 있습니다.



### [Improving fairness, efficiency, and stability in http-based adaptive video streaming with festive](https://conferences.sigcomm.org/co-next/2012/eproceedings/conext/p97.pdf)

#### Abstract

 많은 상업적인 비디오 플레이어들은 네트워크 환경의 변화에 따라 비트레이트를 변화시키는 로직을  필요로 합니다. 과거 측정 연구들은 여러 비트레이터 적응형 플레이어들이 병목 현상을 겪는 것을 보고 효율성, 공정성 및 안정성 3가지 핵심지표에 대해 상업용 플레이어와 관련된 문제를 파악했습니다. 안타깝게도 우리의 현재 이해 상황에서는 이러한 효과가 발생하는 이유와 완화 방법에 대한 이해가 부족합니다. 이 논문에서 우리는 비트레이트 적응에 대한 원칙적인 이해를 제시하고 추상 플레이어 모델을 통해 여러 상업적인 플레이어들을 분석합니다. 이 프레임워크를 통해 HTTP에서 비디오 비트레이트 적응을 오버레이한 결과로 발생하는 몇 가지 바람직하지 않은 상호 작용의 근본 원인을 확인합니다. 이러한 확인을 통해서 안전성과 공정성 및 효율성 간의 균형을 체계적으로 안내합니다. 그리고 이는 견고한 비디오 적응을 위한 프레임워크가 됩니다. 우리는 공간을 설계하고 현재 상업적인 사용자들에게 다양한 실험 시나리오를 경험하게 하였고 이를 통해 3가지 요소에 대한 성능이 크게 증가한 명확한 사례를 보여줍니다.



## 영단어 정리

Pervasive : 만연한

by means of : ~의 도움으로

typically : 일반적으로

experimental : 실험적인

commercial : 상업적인

rely on : ~을 필요로 하다

identify : 확인하다

bottleneck : 좁은 도로, 병목 지역

principled : 절조 있는

several : 각각의

undesirable : 원하지 않는

insight : 이해

robust : 견고한 

lead to : ~로 이어지다

instance : 사례, 경우

significantly : 상당히, 중요하게

outperform : 더 나은 결과를 내다

converge : 집중되다



## To-Do List

- 디지털 신호처리 3강
- **소프트웨어 공학 개론 (8~12강) (+5)**