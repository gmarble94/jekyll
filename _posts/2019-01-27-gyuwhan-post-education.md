---
layout: post
title:  "Gitlab에 포스팅 하는법 for CTD"
date:   2019-01-27 00:00:00 0000
author : "Gyuhwan Kim"
categories: education
mathjax : true
---

1 ) 저장할 경로에서 terminal 혹은 cmd창을 켜고 git clone https://gitlab.com/gmarble94/jekyll.git 입력

2 ) 마크다운 에디터를 켠다. (여기선 [Typora](https://typora.io/)를 사용하였다.)

3 ) 먼저, --- 을 입력하고 엔터를 친다. 그러면 회색으로 네모난 창이 생기면서 input YAML Front Matter. 이라고 뜬다.

4 ) 이 부분에 

layout: post   
title:  "Gitlab에 포스팅 하는법"   
date:   2019-01-27 00:00:00 0000   
author : "Gyuhwan Kim"   
categories: education   
mathjax : true  

위와 같이 입력한다. 

- 여기서 layout은 post로 고정
- title은 글의 제목
- date는 시간은 표시안되기 떄문에, 그냥 날짜만 맞게 변경해주면 된다.
- author은 각자 이름에 맞게 "Gyuhwan Kim" or "Rojin Jung" or "Hakkyu Kim" 으로 적어주면 된다.
- categories도 자유롭게 작성
- mathjax : true는 수식을 사용해야 할 때 사용

작성하면 아래와 같이 나온다.

![포스팅 이미지 01](/jekyll/assets/img/2019-01-27-gyuhwan-gitlab-post-01.png)

5 ) 이제 마크다운 편집기에 맞게 작성한다.

6 ) [수식을 작성할 경우](https://gmarble94.gitlab.io/jekyll/education/2019/02/09/gyuhwan-education.html)  

```none
\\[
x(t) = Acos(\omega_0t+\phi) =Acos(2\pi f_0t+\phi)
\\]
```

~~와 같이 작성하면 된다. 마크다운 편집기에서는 이상하게 나오지만, 실제 블로그에 올라가면 아래와 같이 제대로 나온다.~~

~~![포스팅 이미지 02](/jekyll/assets/img/2019-01-27-gyuhwan-gitlab-post-02.png)~~

~~\omega 등의 수식은 latex문법과 같다. 필요 시 인터넷 검색하여 사용~~

 **[해당 내용은 2019. 02. 09에 수정되었습니다]**

7 ) 이미지 넣고 싶은 경우 assets/img/ 경로에 이미지 파일을 넣고

```
![이미지 이름](/jekyll/assets/img/추가된 파일.[jpeg (or) png 등등]){:class="img-responsive"}
```

위와 같이 입력하면 업로드 했을 시 제대로 나오게 된다.

8 ) 위와 같이 작성한 후 git clone했던 폴더에서 아래와 같은 순서로 작성하면 된다.

- **git pull (이미 작성된 코드 읽어오기)**
- git add .  (새로 추가되었거나 삭제되었거나 수정된 파일 추가)
- git commit -m "commit log name" (commit 시에 log)
- git push

9 ) [https://gitlab.com/gmarble94/jekyll/pipelines](https://gitlab.com/gmarble94/jekyll/pipelines) 이 링크에서 commit log와 일치하는 작업이 passed되면 블로그에도 업로드 된다. 새로고침(F5)으로 확인